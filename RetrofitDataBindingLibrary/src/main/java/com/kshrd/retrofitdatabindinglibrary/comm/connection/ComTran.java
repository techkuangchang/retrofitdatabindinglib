package com.kshrd.retrofitdatabindinglibrary.comm.connection;

import android.content.Context;
import android.util.Log;

import com.kshrd.retrofitdatabindinglibrary.comm.network.RetrofitNetwork;
import com.kshrd.retrofitdatabindinglibrary.comm.network.internal.OnNetworkListener;

import org.json.JSONObject;

import java.net.URLDecoder;
import java.util.HashMap;

/**
 * Useful communication mobile to server, using retrofit network
 * request & respond data
 *
 * @author KOSIGN Smart
 * @since 2019. 11. 29
 */
public class ComTran implements OnNetworkListener {
    private Context mContext;
    private RetrofitNetwork mRetrofitNetwork;

    /**
     * init
     * @param context activity context
     * @
     */
    public ComTran(Context context , OnComTranListener listener) {
        mContext = context;
        mRetrofitNetwork = new RetrofitNetwork(mContext, this);
        mRetrofitNetwork.setTimeOut(60 * 1000 * 5);
        mRetrofitNetwork.setReadTimeOut(30 * 1000 * 5);
    }

    /**
     * MG 전문 호출
     * @param tranCd MG code service
     * @param url MG url
     */
    public void requestData(String tranCd, String url) {
        HashMap<String, String> params = new HashMap<>();
        params.put("master_id", "A_NEW_BIPLE_G_1");
        mRetrofitNetwork.requestRetrofitNetwork(tranCd, true, url , params);
    }

    public void requestData(String tranCd, String request_data, boolean method) {
        HashMap<String, String> params = new HashMap<>();
        params.put("master_id", "A_NEW_BIPLE_G_1");
        mRetrofitNetwork.requestRetrofitNetwork(tranCd, method, request_data , params);
    }

    @Override
    public void onNetworkResponse(String tranCd, Object object) {
        JSONObject jsonOutput;
        try {
            jsonOutput = new JSONObject(URLDecoder.decode(object.toString(), "UTF-8"));
            Log.d(">>> ", "jsonOut:: " + jsonOutput.toString(1));
        }catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onNetworkError(String tranCode, Object object) {

    }

    /**
     *  통신 처리 UI단 전달 Listener
     */
    public interface OnComTranListener {
        void onTranResponse(String tranCd, Object object);
        void onTranError(String tranCd, Object object);
    }
}
