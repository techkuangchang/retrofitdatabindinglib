package com.kshrd.retrofitdatabindinglibrary.comm.network.retrofit;

import java.util.Map;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.HeaderMap;
import retrofit2.http.Multipart;
import retrofit2.http.PATCH;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.PartMap;
import retrofit2.http.QueryMap;
import retrofit2.http.Url;

public interface RetrofitService {

    /**
     * get method
     */
    @GET
    Call<ResponseBody> getRequestMessage(@HeaderMap Map<String, String> headers, @Url String url, @QueryMap Map<String, String> requestData);

    /**
     * post method
     */
    @FormUrlEncoded
    @POST
    Call<ResponseBody> postRequestMessage(@HeaderMap Map<String, String> headers, @Url String url, @FieldMap Map<String, String> requestData);

    /**
     * post method with body
     */
    @POST
    Call<ResponseBody> postRequestMessageBody(@HeaderMap Map<String, String> headers, @Url String url, @Body Map<String, String> requestData);

    /**
     * post method with body
     */
    @POST
    Call<ResponseBody> postRequestMessageBody(@HeaderMap Map<String, String> headers, @Url String url, @Body RequestBody requestData);

    /**
     * put method with body
     */
    @PUT
    Call<ResponseBody> putRequestMessageBody(@HeaderMap Map<String, String> headers, @Url String url, @Body RequestBody requestData);

    /**
     * patch method with body
     */
    @PATCH
    Call<ResponseBody> patchRequestMessageBody(@HeaderMap Map<String, String> headers, @Url String url, @Body RequestBody requestData);

    /**
     * post method with multi part
     */
    @Multipart
    @POST
    Call<ResponseBody> postRequestMultipart(@HeaderMap Map<String, String> headers, @Url String url, @PartMap Map<String, MultipartBody> requestData);

    /**
     * put method with multi part
     */
    @Multipart
    @PUT
    Call<ResponseBody> putRequestMultipart(@HeaderMap Map<String, String> headers, @Url String url, @PartMap Map<String, MultipartBody> requestData);
}
